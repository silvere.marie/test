configuration = [
  {
    name    = "name1",
    value   = "value1",
    encrypt = true
  },
  {
    name    = "name2",
    value   = "value2",
    encrypt = false
  },
  {
    name    = "name3",
    value   = "value3",
  }
]

service_name = "service_name"

encrypt_key_id = "encrypt_key_id"