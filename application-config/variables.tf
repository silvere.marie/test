variable "configuration" {
  type = list(object({
    name    = string,
    value   = string,
    encrypt = optional(bool)
  }))
}

variable "encrypt_key_id" {
  type    = string
  default = null
}

variable "environment" {
  type    = string
  default = null
}

variable "service_name" {
  type = string
}
