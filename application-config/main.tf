terraform {
  experiments = [module_variable_optional_attrs]
}

locals {
  service_name_suffix = var.environment == null ? "" : "-${var.environment}"
  context             = "${var.service_name}${local.service_name_suffix}"
}

resource "aws_ssm_parameter" "ssm_parameter" {
  count  = length(var.configuration)
  name   = "/config/${local.context}/${var.configuration[count.index].name}"
  type   = var.configuration[count.index].encrypt == true ? "SecureString" : "String"
  value  = var.configuration[count.index].value
  key_id = var.encrypt_key_id
}

